To run the code:
-Open up terminal
-Browse to project location
-Type "gcc main.c src/*.c -o out"

The main function is located in the project root directory.

Source files are located in src folder. These are:
menus.h => Contains definitions for menu print and selection functions
war.h => Contains definitions for soldier and army structs, and game related functions
soldiers.txt => Contains soldier properties. Name, attack type, attack power, and stamina in this exact sequence. For attack type, numbers 0,1, and 2 represent Sword, Fire, and Ice, respectively.

Upon execution, first the function "load_soldiers" is called with the parameter file name, which then looks up to the src directory for a file called "soldiers.txt". This function reads lines from this text file and saves the soldier properties to an array of "soldier type" (soldier_t).

If the file does not exist or the read operation cannot be completed otherwise, then the program exits with code 1 (EXIT_FAILURE). Otherwise, the function "print_menu" is called.

The function "print_menu" prints the main menu as shown:
"Please select an option to continue:
 1) Show soldiers' features
 2) Start the war
 Please make your choice:"

In any case, the function "execute_selection" is called using the user input as argument.
This function takes two arguments. The menu type, which is an enumerated value with options MAIN_MENU and WESTEROS, and the user selection as a character. It then executes user selection with respect to the chosen menu type. 
If "esc" is pressed in Main Menu, the program exits. If "esc" is pressed in Westeros, the program returns to Main Menu.

On the main menu:

-If the user presses "1", the function "show_feats" is called followed by a subsequent call to "print_menu".
"show_feats" asks the user for a string and prints the features of the soldier (if exists) with the given name.

-If the user presses "2", the function "start_war" is called. If the global variable "begin" is set to true AND there are sufficient soldiers in the player's army, the function "begin_war" defined in "war.h" is called. Otherwise, the following menu is printed followed by a call to "execute_function" with arguments "WESTEROS" as menu type and the input taken from user.

"Welcome to Westeros. How can I help you?"
 1) Show soldiers"
 2) Select your soldiers"
 3) Release a soldier"
 4) Show my army"
 5) Back"
 Please make your choice:"

On the Westeros menu:

-If the user presses "1", the function "list_soldiers" is called. This function list the names of all soldiers read from the file.

-If the user presses "2", the function "select_soldier" is called. This function prompts the user for an input if the maximum soldier count is not reached yet, and adds the soldier with the corresponding id to the army struct called "player_army".

-If the user presses "3", the function "release_soldier" is called which prompts the user for input and calls remove_from_array to remove the soldier from the army.
The function "remove_from_array" takes two arguments, the army to remove from, and the id of the soldier to remove. Returns true upon success and false if no soldier with the given id is found.

-If the user presses "4", the function "list_army" is called with the pointer player_army as argument. This function lists the names of all soldiers in the army. If the second argument is true, fetatures of the soldiers are printed as well.

-If the user presses "5", the global bool variable "begin" will become true, and the function returns by calling "print_menu".

In cases 1,2,3, and 4, the function "start_war" is called which either starts the game or prints the Westeros menu once again. 

The struct "soldier_struct" holds the following elements:

-string "name": Name of the soldier
-int "attack_power": Attack power of the soldier
-int "stamina": Stamina of the soldier
-int "id": ID of the soldier
-attack_t "attack": Attack type of soldier as defined in the enum "attack"

The struct "army" holds the following elements:

-Array of the struct soldier_struct: Soldiers in the army
-int "size": How many soldiers are there in the army

If the user chooses "Back" (5) on WESTEROS menu and if there are at least 2 soldiers in their army and chooses the option "Start war" (2), then the function "begin_war" is called.

"begin_war": Calls the following functions to prepare AI and battlefield; "deploy_ai_army" and "print_turn_menu".
Then the function "make_move" is called with the argument alternating between player and ai. After each turn, the function "check_game_over" is called to check the status. If it return true, the program exits with code 0.

"deploy_ai_army" randomly chooses four different soldiers to add to the ai_army. Soldiers already in player_army are excluded.

"print_turn_menu" prints the battlefield along with the soldiers in armies.

"check_game_over" checks if each player has soldiers remanining in their respective armies.

"make_move" takes one argument. True indicates the player's turn and false indicates the AI's turn. During player's turn, it asks input from the user to determine the battling soldier this turn. The global soldier_t pointer "player_select" stores the address of this soldier.
During the AI's turn, AI chooses a soldier to fight with the opponent's chosen soldier. This selection is random as there are no type advantages currently implemented.
After AI chooses its soldier, both soldiers lose stamina equal to the attack of the other. If a soldier's stamina is reduced to or below 0, it is removed from the army with the function "remove_from_array".

The game ends if the size of one of the armies reaches 0.