//********************************//
//         BIL 132 Project        //
//                                //
// Author: Omer Zahid Basarkan    //
// Student ID: 200011703          //
// Date: 08/06/2020               //
// menus.h                        //
//********************************//

#ifndef _MENUS_H
#define _MENUS_H

#include <string.h>
#include "war.h"

typedef enum menu{
    MAIN_MENU,
    WESTEROS
}menu_t;

void print_menu(void);
void execute_selection(menu_t,char);
void show_feats(void);
void start_war(void);
char* enum_to_str(attack_t);

#endif
