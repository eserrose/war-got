#include "war.h"

soldier_t *soldiers, *player_select, *ai_select;
army_t player_army = {.size = 0}, ai_army = {.size = 0};

/**
 * @brief List the names of all soldiers read from the file.
 * 
 */
void list_soldiers(){
    for(int i = 0; i < NUM_SOLDIERS; i++)
        printf("%d. %s\n",soldiers[i].id,soldiers[i].name);
}

/**
 * @brief Lists the names of all soldiers in the army. If stats is true, fetatures of the soldiers are printed as well.
 * 
 * @param army 
 * @param stats 
 */
void list_army(army_t *army, bool stats){
    if(!army->size){
        printf("No soldiers to show.\n");
        return;
    }
    if(stats){
        for(int i = 0; i < army->size; i++)
            printf("# %d-%s (%s,%d,%d)\n",army->soldiers[i].id,army->soldiers[i].name,enum_to_str(army->soldiers[i].attack),army->soldiers[i].attack_power,army->soldiers[i].stamina);
    }
    else{
     for(int i = 0; i < army->size; i++)
        printf("%d. %s\n",army->soldiers[i].id,army->soldiers[i].name);
    }
}

/**
 * @brief If the maximum soldier count is not reached yet, the user is prompted for an input and the soldier with the corresponding id is recruited in the player's army.
 * 
 */
void select_soldier(){

    if(! (player_army.size < MAX_ARMY_SIZE)){
        printf("You already have max amount of soldiers.\n");
        return;
    }  

    list_soldiers();
    printf("Please type the id of the soldier to select it: ");

    int index = get_user_input();

    for(int i = 0; i < player_army.size; i++){
        if(soldiers[index].id == player_army.soldiers[i].id){
            printf("You already have %s in your army, select another.\n", soldiers[index].name);
            return select_soldier();
        }
    }

    player_army.soldiers[player_army.size++] = soldiers[index];
    printf("%s selected\n", soldiers[index].name);

}

/**
 * @brief Prompts the user for input and calls remove_from_array to remove the soldier from the army
 * 
 */
void release_soldier(){

    list_army(&player_army, false);
    printf("Please type the id of the soldier to release it: ");
    
    int index = get_user_input();

    if(!remove_from_army(&player_army, index)){
        printf("You do not have that in your army\n");
        return;
    }

    printf("%s released\n", soldiers[index].name);

}

/**
 * @brief 
 * 
 * @param army 
 * @param index 
 * @return true, upon success.
 * @return false, if the army does not contain a soldier with the given id 
 */
bool remove_from_army(army_t *army, int index){

    for(int i = 0; i < army->size; i++){
        if(index == army->soldiers[i].id){
            soldier_t temp = army->soldiers[army->size-1];
            army->soldiers[army->size-1] = army->soldiers[i];
            army->soldiers[i] = temp;
            army->size--;
            return true;
        }
    }
    return false;
}

/**
 * @brief Get the user input. Prevents non-integer inputs and inputs out of range. Calls itself until the correct input is given.
 * 
 * @return int 
 */
int get_user_input(){

    int index;

    if (!scanf("%d",&index) || index >= NUM_SOLDIERS || index < 0)
    {
        scanf("%*[^\n]"); //discard that line up to the newline
        printf("Wrong input, try again!\n");
        return get_user_input();
    }

    return index;
}

/**
 * @brief Prepares the AI, then calls "make_move" until the game ends.
 * 
 */
void begin_war(){

    bool gameOver = false, activePlayer = false;

    deploy_ai_army();
    print_turn_menu();

    while (!gameOver)
    {
        make_move(activePlayer = !activePlayer);
        gameOver = check_game_over();
    }

    printf("Game Over! Thanks for playing.\n");
    exit(EXIT_SUCCESS);
}

/**
 * @brief Randomly chooses four different soldiers to add to the ai_army.
 * Soldiers already in player_army are excluded
 */
void deploy_ai_army(){
    
    //Randomly choose 4 soldiers for AI. 
    srand(time(0));
    for(int i=0; i < MAX_ARMY_SIZE; i++){
        //Make sure that the same soldier does not get chosen twice.
        bool flag;
        int randIndex;
        do{
            flag = false;
            randIndex = rand() % NUM_SOLDIERS;
            for(int j = 0; j < ai_army.size; j++){
                if(randIndex == ai_army.soldiers[j].id) 
                    flag = true;
            }
            for(int j = 0; j < player_army.size; j++){
                if(randIndex == player_army.soldiers[j].id) 
                    flag = true;
            }
        }while(flag);

        ai_army.soldiers[ai_army.size++] = soldiers[randIndex];
    }
    
}

/**
 * @brief Prints the menu of war
 * 
 */
void print_turn_menu(){
    
    printf("\n...........The Winter is Coming.............\n"
            "#############################################\n"
            "\t\t User \t\t\n\n");

    list_army(&player_army, true);

    printf("\n#############################################\n"
        "#############################################\n");
    
    list_army(&ai_army, true);

    printf("\n\t\t AI \t\t\n"
    "#############################################\n");
    
}

/**
 * @brief Player and AI both chooses a soldier to fight.
 * 
 * @param player 
 */
void make_move(bool player){

    bool flag = false;
    if(player){
        printf("Select the soldier to attack: ");
        int index = get_user_input();

        for(int i = 0; i < player_army.size; i++){
            if(index == player_army.soldiers[i].id){
                printf("%s is selected\n", soldiers[index].name);
                player_select = &player_army.soldiers[i];
                flag = true;
            }
        }
        if(!flag){
            printf("Wrong id!\n");
            return make_move(player);
        }
    }

    else{
        srand(time(0));
        ai_select = &ai_army.soldiers[ (int) (rand() % ai_army.size)];
        printf("AI selected %s\n", ai_select->name);
        printf("%s vs %s\n",player_select->name, ai_select->name);

        player_select->stamina -= ai_select->attack_power;
        ai_select->stamina -= player_select->attack_power;

        if(player_select->stamina <=0) remove_from_army(&player_army,player_select->id);
        if(ai_select->stamina <=0) remove_from_army(&ai_army,ai_select->id);
        print_turn_menu();
    }
}

/**
 * @brief Check if each player has soldiers remanining in their respective armies.
 * 
 * @return true 
 * @return false 
 */
bool check_game_over(){
    return !player_army.size || !ai_army.size;
}
