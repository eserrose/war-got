#include "menus.h"

bool begin = false;

/**
 * @brief Prints main menu, then expects user input to call execute_function
 * 
 */
void print_menu(){
    printf("Please select an option to continue:\n"
        "1) Show soldiers' features \n"
        "2) Start the war \n"
        "Please make your choice:\n");
    execute_selection(MAIN_MENU, getch());
}

/**
 * @brief If the global variable "begin" is set to true AND there are sufficient soldiers in the player's army, calls begin_war.
 * Otherwise, the Westeros menu is called and the user is prompted for input.
 * 
 */
void start_war(){

    if(begin && player_army.size >= MIN_ARMY_SIZE)
        begin_war();

    printf("Welcome to Westeros. How can I help you?\n"
            "1) Show soldiers\n"
            "2) Select your soldiers\n"
            "3) Release a soldier\n"
            "4) Show my army\n"
            "5) Back\n"
            "Please make your choice:\n");
    execute_selection(WESTEROS, getch());

}

/**
 * @brief Executes user selection with respect to the chosen menu type
 * 
 * @param m (menu_t)
 * @param s (char)
 */
void execute_selection(menu_t m,char s){

    if(m == MAIN_MENU){
        if(s == '1'){
            show_feats();
            print_menu();
        }
        else if(s == '2') 
            start_war();
        else if(s == 27)
            exit(0);
        else{
            printf("Incorrect choice, try again.\n");
            print_menu();
        }
    }
    else if(m == WESTEROS){
        switch(s){
            case 27:
                print_menu();
                break;
            case '1':
                list_soldiers();
                break;
            case '2':
                select_soldier();
                break;
            case '3':
                release_soldier();
                break;
            case '4':
                list_army(&player_army, false);
                break;
            case '5':
                begin = true;
                print_menu();
            default:
                break;
        }
        start_war();
    }
}

/**
 * @brief Asks the user for a string and prints the features of the soldier (if exists) with the given name
 * 
 */
void show_feats(){
    char name[MAX_NAME_LEN];
    printf("Please type name of the soldier (type 0 to close and return to the main menu): \n");
    scanf("%s",name); 
    if(*name == '0') return print_menu();
    else{
         for (int i = 0; i < NUM_SOLDIERS; i++)
        { 
           if (strncmp(soldiers[i].name, name, strlen(name)) == 0)
                printf("Name: %s\nAttack: %s\nAttack Power: %d \nStamina: %d\n",soldiers[i].name,enum_to_str(soldiers[i].attack),soldiers[i].attack_power,soldiers[i].stamina);
        }
    }
    
}

/**
 * @brief Used for printing the attack type of the soldier 
 * 
 * @param attack 
 * @return char* 
 */
char* enum_to_str(attack_t attack){
    switch(attack){
        case Sword:
            return "Sword";
        case Fire:
            return "Fire";
        case Ice:
            return "Ice";
    }
    return "";
}

