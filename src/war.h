//********************************//
//         BIL 132 Project        //
//                                //
// Author: Omer Zahid Basarkan    //
// Student ID: 200011703          //
// Date: 08/06/2020               //
// war.h                          //
//********************************//
#ifndef _WAR_H
#define _WAR_H

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <conio.h>
#include <time.h>

#define MAX_SOLDIERS 255
#define NUM_SOLDIERS 12
#define MAX_NAME_LEN 20
#define MAX_ARMY_SIZE 4
#define MIN_ARMY_SIZE 2

typedef enum attack{
    Sword,
    Fire,
    Ice
} attack_t;

typedef struct soldier_struct {
    char name[MAX_NAME_LEN];
    attack_t attack;
    int attack_power;
    int stamina;
    int id;
} soldier_t;

typedef struct army{
    soldier_t soldiers[MAX_ARMY_SIZE];
    int size;
} army_t;

extern army_t player_army, ai_army;
extern soldier_t *soldiers;
extern void print_menu();
extern char* enum_to_str(attack_t);

void list_soldiers();
void list_army(army_t*, bool);
void select_soldier();
void release_soldier();
void begin_war();
void make_move(bool);
void deploy_ai_army();
void print_turn_menu();
bool remove_from_army(army_t*,int);
bool check_game_over();
int get_user_input();


#endif
