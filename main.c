//********************************//
//         BIL 132 Project        //
//                                //
// Author: Omer Zahid Basarkan    //
// Student ID: 200011703          //
// Date: 08/06/2020               //
// main.c                         //
//********************************//


#include "./src/menus.h"
#include "./src/war.h"

#define BUFFER_LEN 255

void load_soldiers(char*);

int main(){
    load_soldiers("src/soldiers.txt");
    print_menu();
}

/**
 * @brief Reads the file "filename" and stores the elements as soldier_t in soldiers array
 * @param filename
 */
void load_soldiers(char* filename){

    soldiers = malloc(MAX_SOLDIERS*sizeof(*soldiers));
    int i = 0;

    FILE *fptr;
    char buffer[BUFFER_LEN];

    int tmp;

    //Open file. Exit if no file found.
    if ((fptr = fopen(filename,"r")) == NULL){
        printf("Error! opening file");
        exit(1);
    }

    while(fgets(buffer, BUFFER_LEN, fptr)) {
        sscanf(buffer,"%[^,],%d,%d,%d",soldiers[i].name,(attack_t)&soldiers[i].attack,&soldiers[i].attack_power,&soldiers[i].stamina);
        soldiers[i].id = ++i;
    }

    soldiers = realloc(soldiers,i*sizeof(*soldiers));

   fclose(fptr);
}